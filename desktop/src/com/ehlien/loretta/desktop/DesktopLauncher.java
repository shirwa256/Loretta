package com.ehlien.loretta.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.ehlien.loretta.swingcopters.Loretta;

public class DesktopLauncher {
	public static void main(String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Mad Copter";
		config.width = 272;
		config.height = 408;
		new LwjglApplication(new Loretta(new ActionResolverDesktop()),
				config);
	}
}
