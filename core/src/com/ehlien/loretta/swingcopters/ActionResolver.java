package com.ehlien.loretta.swingcopters;

/**
 * Created by DevStation on 2016-05-19.
 */
public interface ActionResolver {

    public void showOrLoadInterstital();

    public void signIn();

    public void signOut();

    public void rateGame();

    public void submitScore(long score);

    public void showScores();

    public boolean isSignedIn();
}
