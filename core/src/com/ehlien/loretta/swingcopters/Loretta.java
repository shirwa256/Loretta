package com.ehlien.loretta.swingcopters;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.ehlien.loretta.helpers.AssetLoader;
import com.ehlien.loretta.screens.SplashScreen;

public class Loretta extends Game {
	private ActionResolver actionResolver;

	public Loretta(ActionResolver actionResolver) {
		this.actionResolver = actionResolver;

	}

	@Override
	public void create() {
		Gdx.app.log("Game", "Created");
		AssetLoader.load();
		setScreen(new SplashScreen(this, actionResolver));

	}

	@Override
	public void dispose() {
		super.dispose();
		AssetLoader.dispose();
	}
}
