package com.ehlien.loretta.gameobjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.ehlien.loretta.gameworld.GameWorld;
import com.ehlien.loretta.helpers.AssetLoader;

import java.util.Random;

/**
 * Created by DevStation on 2016-05-19.
 */
public class Block extends Scrollable {

    private Random r;
    private int gap = 70;
    private boolean tp=false;
    private GameWorld world;
    private Rectangle rRectangle;
    private boolean isScored = false;
    private boolean isPressed = false;
    private int colourCode=0;

    public Block(GameWorld world, float x, float y, int width, int height,
                 float scrollSpeed) {
        super(world, x, y, width, height, scrollSpeed);
        this.world = world;
       // r = new Random();
        //this.width = r.nextInt((int) (world.gameWidth - gap - 10 - 10)) + 10;
        // Initialize a Random object for Random number generation

        //lRectangle = new Rectangle(position.x, position.y, this.width, height);
        rRectangle = new Rectangle(position.x, position.y,
                width, height);
    }

    @Override
    public void update(float delta) {
        super.update(delta);
       // lRectangle = new Rectangle(position.x, position.y, width, height);
        rRectangle = new Rectangle(position.x, position.y,
                width, height);
    }

    public boolean touchTp(){

        if(isPressed==true){
            tp=true;
            return tp;
        }

        return false;
    }

    @Override
    public void reset(float newY) {
        // Call the reset method in the superclass (Scrollable)
        super.reset(newY);
        //width = r.nextInt((int) (world.gameWidth - gap - 10)) + 10;
        isScored = false;
        // Change the height to a random number
    }

   /* public boolean collides(Copter copter) {
        if (position.y < copter.getY() + copter.getWidth()) {
            return (Intersector.overlaps(copter.getRectangle(), rRectangle) || Intersector
                    .overlaps(copter.getRectangle(), lRectangle));
        }
        return false;

    }*/

    public int getGap() {
        return gap;
    }

  //  public Rectangle getLRectangle() {
    //    return lRectangle;
    //}

    public Rectangle getRRectangle() {
        return rRectangle;
    }

    public boolean isScored() {
        return isScored;
    }

    public void setScored(boolean b) {
        isScored = b;
    }

    public void onRestart(float y, int scrollSpeed) {
        isScored = false;
        velocity.y = scrollSpeed;

        reset(y);
       // lRectangle = new Rectangle(position.x, y, this.width, height);
        rRectangle = new Rectangle(position.x, y,
                width, height);

    }

    public void setTp(){
        tp=false;

    }

    public void onClick() {


        world.addScore(1);
        touchTp();
        isPressed = false;

        System.out.println("Block has been touched");
        System.out.println(colourCode);
    }

    public boolean isTouchDown(int screenX, int screenY) {

        if (rRectangle.contains(screenX, screenY)) {

            isPressed = true;
            onClick();

            return true;
        }


        return false;
    }

    public boolean isTouchUp(int screenX, int screenY) {

        // It only counts as a touchUp if the button is in a pressed state.
        if (rRectangle.contains(screenX, screenY) && isPressed) {
            isPressed = false;
            return true;
        }

        // Whenever a finger is released, we will cancel any presses.
        isPressed = false;
        return false;
    }

    public void matchingCode(int x){
        this.colourCode=x;
    }
}
