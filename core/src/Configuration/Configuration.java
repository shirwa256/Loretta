package Configuration;

public class Configuration {

	// Admob Ad ID
	public static final String AD_UNIT_ID_BANNER = "ca-app-pub-8005608357319897/3761655162";
	// Admob Interestial ID
	public static final String AD_UNIT_ID_INTERSTITIAL = "ca-app-pub-8005608357319897/6715121569";
	// Show interestial Ads every X gameovers
	public static final int INTERESTIAL_FREQ = 1; // Number
	// Shows Ads "UP" or "DOWN" (on top or bottom)
	public static final String AD_POSITION = "UP"; // UP or DOWN

	// Id of the highscores leaderboard in Google Play Store
	public static final String PLAY_LEADERBOARD_ID = "CgkIiNn4ytYPEAIQAQ";
	// Go to the android folder > res > values > strings
	// and put the highscore leaderbaord id there

	public static final boolean LEADERBOARDS = true;

}
